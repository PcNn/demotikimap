//
//  ViewController.h
//  DemoTikiMap
//
//  Created by Nguyen Phuc on 12/31/15.
//  Copyright © 2015 Nguyen Phuc. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface ViewController : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate>{
    UITextField* activetextField;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIView *locationAView;
@property (strong, nonatomic) IBOutlet UIView *locationBView;
@property (strong, nonatomic) IBOutlet UITextField *txtA;
@property (strong, nonatomic) IBOutlet UITextField *txtB;

@end

