//
//  ViewController.m
//  DemoTikiMap
//
//  Created by Nguyen Phuc on 12/31/15.
//  Copyright © 2015 Nguyen Phuc. All rights reserved.
//
//Demo
#import "ViewController.h"
#import "MDDirectionService.h"

@interface ViewController (){
    UITapGestureRecognizer* tapRecognizer;
    CLLocationManager* locationManager;
    GMSMarker* markerA;
    GMSMarker* markerB;
    NSString* wayPointStrA;
    NSString* wayPointStrB;
    
    BOOL isTextFieldA;
    
    GMSPolyline *polyline;
}
@property (nonatomic, retain) CLLocation* initialLocation;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    [self configView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (self.initialLocation==nil) {
        self.initialLocation = newLocation;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                longitude:newLocation.coordinate.longitude
                                                                     zoom:14.0];
        [self.mapView animateToCameraPosition:camera];
    }
}

-(void)getPositionWithLatitude:(double)lat Longtitude:(double)lng AdressStr:(NSString*)address withMarker:(BOOL)isMarkerA{
    if (isMarkerA) {
        if (markerA) {
            markerA.position = CLLocationCoordinate2DMake(lat, lng);
            CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            
            [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
                if(address) {
                    markerA.title = address;
                }
            }];
            markerA.map = self.mapView;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                    longitude:lng
                                                                         zoom:16.0];
            [self.mapView animateToCameraPosition:camera];
        } else{
            markerA = [[GMSMarker alloc] init];
            [markerA setDraggable:YES];
            markerA.position = CLLocationCoordinate2DMake(lat, lng);
            CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            
            [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
                if(address) {
                    markerA.title = address;
                }
            }];
            markerA.map = self.mapView;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                    longitude:lng
                                                                         zoom:16.0];
            [self.mapView animateToCameraPosition:camera];
        }
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",lat,lng];
        wayPointStrA = positionString;
        
    } else{
        if (markerB) {
            markerB.position = CLLocationCoordinate2DMake(lat, lng);
            CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            
            [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
                if(address) {
                    markerB.title = address;
                }
            }];
            markerB.map = self.mapView;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                    longitude:lng
                                                                         zoom:16.0];
            [self.mapView animateToCameraPosition:camera];
        } else{
            markerB = [[GMSMarker alloc] init];
            [markerB setDraggable:YES];
            markerB.position = CLLocationCoordinate2DMake(lat, lng);
            CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            
            [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
                if(address) {
                    markerB.title = address;
                }
            }];
            markerB.map = self.mapView;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                                    longitude:lng
                                                                         zoom:16.0];
            [self.mapView animateToCameraPosition:camera];
        }
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",lat,lng];
        wayPointStrB = positionString;
    }
    if (wayPointStrA.length>0&&wayPointStrB.length>0) {
        [self makeParameter];
    }

}
-(void)makeParameter{
    NSString *sensor = @"false";
    NSMutableArray* temp = [[NSMutableArray alloc] init];
    [temp addObject:wayPointStrA];
    [temp addObject:wayPointStrB];
    NSArray *parameters = [NSArray arrayWithObjects:sensor, temp,
                           nil];
    NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
    NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                      forKeys:keys];
    MDDirectionService *mds=[[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    [mds setDirectionsQuery:query
               withSelector:selector
               withDelegate:self];
}
- (void)addDirections:(NSDictionary *)json {
    
    polyline.map = nil;
    
    NSDictionary *routes = [json objectForKey:@"routes"][0];
    
    NSDictionary *route = [routes objectForKey:@"overview_polyline"];
    NSString *overview_route = [route objectForKey:@"points"];
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    polyline = [GMSPolyline polylineWithPath:path];
    polyline.map = self.mapView;
}
-(void)configView{
    self.locationAView.backgroundColor = [UIColor whiteColor];
    self.locationBView.backgroundColor = [UIColor whiteColor];
    self.bottomView.backgroundColor = [UIColor colorWithRed:3/255.0f green:182/255.0f blue:240/255.0f alpha:1.0f];
    self.txtA.delegate = self;
    self.txtB.delegate = self;
    [self registerForKeyboardNotifications];
    [self AddTaprecognizerDismissKeyboard];
}
-(void)AddTaprecognizerDismissKeyboard{
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}
-(void)dismissKeyboard:(UITapGestureRecognizer*)senser{
    [self.view endEditing:YES];
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    double lat = marker.position.latitude;
    double lng = marker.position.longitude;
    NSLog(@"lat: %f - long:%f",lat,lng);
    CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
        if(address) {
            marker.title = address;
            if (marker==markerA) {
                self.txtA.text = address;
                NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",lat,lng];
                wayPointStrA = positionString;
            } else{
                self.txtB.text = address;
                NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",lat,lng];
                wayPointStrB = positionString;
            }
            if (wayPointStrA.length>0&&wayPointStrB.length>0) {
                [self makeParameter];
            }
        }
    }];
}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@", placemark.name, placemark.locality];
             completionBlock(address);
         }
     }];
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activetextField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activetextField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
    if (textField==self.txtA) {
        isTextFieldA = YES;
    } else{
        isTextFieldA = NO;
    }
    return YES;
}

    // TODO: Google Place Autocomplete.

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSString* addressStr = place.name;
    if (isTextFieldA) {
        self.txtA.text = addressStr;
        [self getPositionWithLatitude:place.coordinate.latitude Longtitude:place.coordinate.longitude AdressStr:addressStr withMarker:YES];
    } else{
        self.txtB.text = addressStr;
        [self getPositionWithLatitude:place.coordinate.latitude Longtitude:place.coordinate.longitude AdressStr:addressStr withMarker:NO];
    }
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//----------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activetextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    activetextField = nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (self.txtA) {
        if (!self.txtB.text.length>0) {
            [self.txtB becomeFirstResponder];
        } else{
            if (!self.txtA.text.length>0) {
                [self.txtA becomeFirstResponder];
            }
        }
    }
    return YES;
}
@end
