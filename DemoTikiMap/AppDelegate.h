//
//  AppDelegate.h
//  DemoTikiMap
//
//  Created by Nguyen Phuc on 12/31/15.
//  Copyright © 2015 Nguyen Phuc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameController/GameController.h>
@import GoogleMaps;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

