//
//  MDDirectionService.h
//  DemoTikiMap
//
//  Created by Nguyen Phuc on 1/1/16.
//  Copyright © 2016 Nguyen Phuc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDDirectionService : NSObject

- (void)setDirectionsQuery:(NSDictionary *)object withSelector:(SEL)selector
              withDelegate:(id)delegate;
- (void)retrieveDirections:(SEL)sel withDelegate:(id)delegate;
- (void)fetchedData:(NSData *)data withSelector:(SEL)selector
       withDelegate:(id)delegate;

@end
